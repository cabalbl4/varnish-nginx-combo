# Varnish - Nginx combo

Varnish has issues with IP resolution by default (`Backend host "***": resolves to too many addresses.` error). It can not work with multiple IPs under same DNS name (at least free version/default configuration).

Just a quick and dirty attempt to mitigate that with:

Client: GET (varninsh-server|localhost):8080/api/test => GET nginx => GET (your-api.com/api/test)

## Stack

* docker-compose
* varnish:stable
* nginx:stable

# Start

* Edit nginx.conf to replace your-api.com with real server url
* Optional: override origin cahce control with add_header cache-control "public, max-age=86400"; and proxy_hide_header cache-control; (remove if needed)
* docker-compose up
* Open port 8080 locally


### Using port 80 instead of 8080

Replace line `- "8080:80"` with  `- "80:80"` in docker-compose.yaml

# Disclaier

This is a test setup you may want to modify before using in production. Like hide nginx port 3333.